//
//  ProductListUI.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import SwiftUI

struct ProductCatalogListUI: View {
    
    let catalogs: [ProductCatalogViewModel]
    var body: some View {
        List(catalogs) { catalog in
            ProductCatalogCellUI(productCatalog: catalog)
        }.listStyle(.plain)
    }
}
