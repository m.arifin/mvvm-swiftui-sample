//
//  ProductListCellUI.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import SwiftUI

struct ProductCatalogCellUI: View {
    let productCatalog: ProductCatalogViewModel
    var body: some View {
        HStack(spacing: 20) {
            AsyncImage(url: productCatalog.thumbnail) { image in
                image.resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: 100, maxHeight: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
            } placeholder: {
                ProgressView()
            }
            VStack (spacing: 10) {
                Text(productCatalog.title)
                Text(productCatalog.description)
                Text(" \(productCatalog.price)")
            }
            
        }
    }
}
