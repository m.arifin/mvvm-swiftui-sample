//
//  MVVM_SwiftUIApp.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import SwiftUI

@main
struct MVVM_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ProductCatalogListView()
        }
    }
}
