//
//  ProductListViewModel.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import Foundation

@MainActor
class ProductCatalogListViewModel: ObservableObject {
    
    @Published var productCatalogs: [ProductCatalogViewModel] = []
    
    func populateCategories() async {
        
        do {
            let productCatalogResponse = try await Webservice().get(url: Constants.Urls.productURL) { data in
                return try? JSONDecoder().decode(ProductCatalogResponse.self, from: data)
            }
            
            self.productCatalogs = productCatalogResponse.products
                .map(ProductCatalogViewModel.init)
            
        } catch {
            print(error)
        }
        
    }
    
}

struct ProductCatalogViewModel: Identifiable {
    
    let id = UUID()
    private let productCatalog: ProductCatalog
    
    init(_ productList: ProductCatalog) {
        self.productCatalog = productList
    }
    
    var title: String {
        productCatalog.title
    }
    
    var description: String {
        productCatalog.description
    }
    
    var price: Int {
        productCatalog.price
    }
    
    var thumbnail: URL? {
        URL(string: productCatalog.thumbnail)
    }
    
}
