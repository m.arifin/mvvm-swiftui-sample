//
//  Constants.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import Foundation

struct Constants {
    
    struct Urls {
        static let productURL = URL(string: "https://dummyjson.com/products")!
    }
    
}
