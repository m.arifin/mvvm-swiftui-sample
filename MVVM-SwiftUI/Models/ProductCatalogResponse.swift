//
//  ProductList.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import Foundation

struct ProductCatalogResponse: Decodable {
    let products: [ProductCatalog]
}

struct ProductCatalog: Decodable {
    let id: String
    let thumbnail: String
    let title: String
    let description: String
    let price: Int
}
