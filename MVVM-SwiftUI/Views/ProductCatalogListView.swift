//
//  ProductListView.swift
//  MVVM-SwiftUI
//
//  Created by Macbook Air on 02/03/23.
//

import SwiftUI

struct ProductCatalogListView: View {
    
    @StateObject var model: ProductCatalogListViewModel = ProductCatalogListViewModel()
    
    var body: some View {
        NavigationView {
            ProductCatalogListUI(catalogs:  model.productCatalogs)
                .task {
                    await model.populateCategories()
                }
                .navigationTitle("Product")
        }
    }
}

struct ProductCatalogListView_Previews: PreviewProvider {
    static var previews: some View {
        ProductCatalogListView()
    }
}
